// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --

Cypress.Commands.add("login", (email, password) => {
	cy.intercept('POST', '**/auth/login', {
		statusCode: 200,
		body: {
			token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImJlaHJAc2JlLmxvY2FsIiwidXNlcklkIjoiNjFhOGE5MmU0M2Y4ZjMzNDFkY2E4Y2E0IiwiaWF0IjoxNjM5NTcwMTI3LCJleHAiOjE2Mzk2NTY1Mjd9.WSIFtrwX5ovobTCmnW6_hdczmFP6FKcJkRWk6oe5Os8',
			userId: '61a8a92e43f8f3341dca8ca4'
		}
	})
	
	cy.intercept('GET', '**/user', {
		statusCode: 200,
		body: {
			user: {
				displayName: "Cynthia Press",
				email: "test@example.com",
				createdAt: "2021-12-02T11:08:30.767Z",
				updatedAt: "2021-12-14T07:34:25.259Z",
				language: "de-DE"
			}
		}
	})
	
	cy.intercept('GET', '**/session', {
		statusCode: 200,
		body: {
			sessions: []
		}
	})
	
	cy.visit('/tabs/login');
	cy.get('#login_username > input').type(email);
	cy.get('#login_password > input').type(password);
	cy.get('#submit_button').click();
	
	cy.url().should('include', '/user/session');
	cy.get('#tab-button-sessions');
	cy.get('#tab-button-settings');
	
	cy.window().then((window) => {
			expect(window.localStorage.getItem('CapacitorStorage.token')).eq('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImJlaHJAc2JlLmxvY2FsIiwidXNlcklkIjoiNjFhOGE5MmU0M2Y4ZjMzNDFkY2E4Y2E0IiwiaWF0IjoxNjM5NTcwMTI3LCJleHAiOjE2Mzk2NTY1Mjd9.WSIFtrwX5ovobTCmnW6_hdczmFP6FKcJkRWk6oe5Os8')
	});
	
})

Cypress.Commands.add("logout", () => {
	cy.get('#tab-button-settings').click();
	cy.get('#settings_button_logout').click();
	
	cy.url().should('include', '/tabs/join');
	
	cy.window().then((window) => {
		expect(window.localStorage.getItem('CapacitorStorage.token')).to.be.null;
	});
})

//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
