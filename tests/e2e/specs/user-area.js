describe('User Area', () => {
	function fillSessionAddForm() {
		cy.get('#session_add_name > input').type('Neue Session',{ force: true });
		cy.get('#session_add_open').click();
		
		cy.get('#session_add_submit_button').not.disabled;
	}
	
	it('can login', () => {
		cy.login('test@example.com', 'ABCabc123');
	})

	it('has full start page', () => {
		cy.contains('h1', 'Willkommen, Cynthia Press!');
		cy.contains('h2', 'Meine Sessions');
		cy.contains('ion-card-content', 'Sie haben noch keine Sessions.')
		cy.get('#session_list_new_session_button');
	})

	it('can open the add session dialog', () => {
		cy.get('#session_list_new_session_button').click();

		cy.url().should('include', '/user/session/add');
		cy.contains('h1', 'Neue Session');
		cy.get('#session_add_name')
		cy.get('ion-label[for="session_add_name"]')
		cy.get('#session_add_open')
		cy.get('ion-label[for="session_add_open"]')

		cy.get('#session_add_submit_button').disabled;
	})

	it('shows an error if the session cannot be created', () => {
		fillSessionAddForm();

		cy.intercept('PUT', '**/session', {
			statusCode: 500,
		})

		cy.get('#session_add_submit_button').click({force: true});

		cy.get('ion-toast').shadow().find('div.toast-message').contains('Die neue Session konnte nicht angelegt werden.');
		cy.wait(2000);
	})

	it('can logout', () => {
		cy.logout();
	})

	it('can add sessions', () => {
		cy.login('test@example.com', 'ABCabc123');

		cy.visit('/user/session/add');

		fillSessionAddForm();

		cy.intercept('PUT', '**/session', {
			statusCode: 201,
			body: {
				message: "Session created.",
				sessionId: "61b9e7ced3358f22907e8cc2",
			}
		})

		cy.intercept('GET', '**/session/61b9e7ced3358f22907e8cc2', {
			statusCode: 201,
			body: {
				session: {
					"_id": "61b9e7ced3358f22907e8cc2",
					"name": "Neue Session",
					"accessCode": "HP95G4NG",
					"isOpen": true,
					"userId": "61a8a92e43f8f3341dca8ca4",
					"createdAt": "2021-12-15T13:04:14.114Z",
					"updatedAt": "2021-12-15T13:04:14.114Z",
					"questions": []
				}
			}
		})

		cy.intercept('GET', '**/session', {
			statusCode: 200,
			body: {
				sessions: [{
					"_id": "61b9e7ced3358f22907e8cc2",
					"name": "Neue Session",
					"accessCode": "HP95G4NG",
					"isOpen": true,
					"userId": "61a8a92e43f8f3341dca8ca4",
					"createdAt": "2021-12-15T13:04:14.114Z",
					"updatedAt": "2021-12-15T13:04:14.114Z",
					"questions": []
				}]
			}
		})

		cy.get('#session_add_submit_button').click({force: true});

		cy.get('ion-toast').shadow().find('div.toast-message').contains('Neue Session wurde gespeichert!');
		cy.wait(2000);

		cy.url().should('include', '/user/session/61b9e7ced3358f22907e8cc2');
	})

	it('has a complete edit page', () => {
		cy.contains('h1', 'Neue Session');
		cy.get('h1 > ion-icon[name="icon-open"]');
		cy.contains('small', 'Erstellt:');

		cy.contains('h2', 'Freigabe');
		cy.get('#session_edit_accessCode');
		cy.get('#session_edit_accessCode > input').should('have.attr', 'readonly');
		cy.get('ion-label[for="session_edit_accessCode"]');
		cy.get('#session_edit_accessCode > input').should('have.value', 'HP95G4NG');
		cy.get('#session_edit_accessCode_copy');
		cy.get('#session_edit_share_qr_code');
		cy.get('#session_edit_share_access_link')

		cy.contains('h2', 'Fragen');
		cy.contains('ion-card-content', 'Sie haben noch keine Fragen in dieser Session.')
		cy.get('#session_edit_add_question');
		cy.get('.fab-horizontal-end');

		cy.get('ion-fab-list').should('not.have.class', 'fab-list-active');

		cy.get('.fab-horizontal-end').click();
		cy.get('ion-fab-list').should('have.class', 'fab-list-active');
		cy.get('#session_edit_rename_button');
		cy.get('#session_edit_close_button');
		cy.get('#session_edit_delete_button');
		cy.get('#session_edit_open_button').should('not.exist');

		cy.get('.fab-horizontal-end').click();
		cy.get('ion-fab-list').should('not.have.class', 'fab-list-active');
	})

	it('selects the access code', () => {
		cy.get('#session_edit_accessCode').click();
		//cannot check if the text is selected, hmmh

		cy.get('#session_edit_accessCode_copy').click();
		cy.get('ion-toast').shadow().find('div.toast-message').contains('Kopiert!');
		cy.wait(2000);

		cy.get('#session_edit_share_access_link').click();
		cy.get('ion-toast').shadow().find('div.toast-message').contains('Kopiert!');
		cy.wait(2000);
	})

	it('can close the session', () => {
		cy.intercept('PATCH', '**/session/61b9e7ced3358f22907e8cc2', {
			statusCode: 200,
			body: {
				session: {
					"_id": "61b9e7ced3358f22907e8cc2",
					"name": "Neue Session",
					"accessCode": "HP95G4NG",
					"isOpen": false,
					"userId": "61a8a92e43f8f3341dca8ca4",
					"createdAt": "2021-12-15T13:04:14.114Z",
					"updatedAt": "2021-12-15T13:04:14.114Z",
					"questions": []
				}
			}
		});
		cy.get('.fab-horizontal-end').click();
		cy.get('#session_edit_close_button').click();

		cy.get('h1 > ion-icon[name="icon-close"]');
		cy.get('#session_edit_close_button').should('not.exist');
		cy.get('#session_edit_open_button');
	})

	it('can open the session', () => {
		cy.intercept('PATCH', '**/session/61b9e7ced3358f22907e8cc2', {
			statusCode: 200,
			body: {
				session: {
					"_id": "61b9e7ced3358f22907e8cc2",
					"name": "Neue Session",
					"accessCode": "HP95G4NG",
					"isOpen": true,
					"userId": "61a8a92e43f8f3341dca8ca4",
					"createdAt": "2021-12-15T13:04:14.114Z",
					"updatedAt": "2021-12-15T13:04:14.114Z",
					"questions": []
				}
			}
		});
		cy.get('.fab-horizontal-end').click();
		cy.get('#session_edit_open_button').click();

		cy.get('h1 > ion-icon[name="icon-open"]');
		cy.get('#session_edit_close_button');
		cy.get('#session_edit_open_button').should('not.exist');
	})

	it('can toggle rename input', () => {
		cy.get('.fab-horizontal-end').click();
		cy.get('#session_edit_rename_button').click();

		cy.get('#session_edit_change_name');
		cy.get('#session_edit_change_name > input').should('have.value', 'Neue Session');
		cy.get('#session_edit_rename_button');

		cy.get('.fab-horizontal-end').click();
		cy.get('#session_edit_rename_button').click();
		cy.get('#session_edit_change_name').should('not.exist');
	})

	it('can reset the name if user cancel edit mode', () => {
		cy.get('.fab-horizontal-end').click();
		cy.get('#session_edit_rename_button').click();

		cy.get('#session_edit_change_name');
		cy.get('#session_edit_change_name > input').should('have.value', 'Neue Session');
		cy.get('#session_edit_change_name > input').clear({force: true}).type('Ein anderer Name', {force: true});
		cy.get('#session_edit_rename_button');

		cy.get('.fab-horizontal-end').click();
		cy.get('#session_edit_rename_button').click();
		cy.get('#session_edit_change_name').should('not.exist');
		cy.contains('h1', 'Neue Session');
	})

	it('can rename the session', () => {
		cy.intercept('PATCH', '**/session/61b9e7ced3358f22907e8cc2', {
			statusCode: 200,
			body: {
				session: {
					"_id": "61b9e7ced3358f22907e8cc2",
					"name": "Ein anderer Name",
					"accessCode": "HP95G4NG",
					"isOpen": true,
					"userId": "61a8a92e43f8f3341dca8ca4",
					"createdAt": "2021-12-15T13:04:14.114Z",
					"updatedAt": "2021-12-15T13:04:14.114Z",
					"questions": []
				}
			}
		});

		cy.get('.fab-horizontal-end').click();
		cy.get('#session_edit_rename_button').click();

		cy.get('#session_edit_change_name');
		cy.get('#session_edit_change_name > input').should('have.value', 'Neue Session');
		cy.get('#session_edit_change_name > input').clear({force: true}).type('Ein anderer Name', {force: true});
		cy.get('#session_edit_rename_save_button');
		cy.get('#session_edit_rename_save_button').click();

		cy.get('#session_edit_change_name').should('not.exist');
		cy.contains('h1', 'Ein anderer Name');
	})

	it('can delete the session', () => {
		cy.intercept('DELETE', '**/session/61b9e7ced3358f22907e8cc2', {
			statusCode: 204,
		});

		cy.intercept('GET', '**/user', {
			statusCode: 200,
			body: {
				user: {
					displayName: "Cynthia Press",
					email: "test@example.com",
					createdAt: "2021-12-02T11:08:30.767Z",
					updatedAt: "2021-12-14T07:34:25.259Z",
					language: "de-DE",
				}
			}
		})

		cy.get('.fab-horizontal-end').click();
		cy.get('#session_edit_delete_button').click();

		cy.url().should('include', '/user/session');
		cy.contains('h2', 'Meine Sessions');
	})
})