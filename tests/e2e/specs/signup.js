describe('Sign up', () => {
	it('can visit the page', () => {
		cy.visit('/tabs/signup');
		cy.contains('h1', 'Registrierung');
		cy.get('#tab-button-join')
		cy.get('#tab-button-login')
	})
	
	it('contains signup form', () => {
		cy.get('#register_username')
		cy.get('ion-label[for="register_username"')
		cy.get('#register_password')
		cy.get('ion-label[for="register_password"')
		cy.get('#register_password_repeat')
		cy.get('ion-label[for="register_password_repeat"')
		cy.get('#register_display_name')
		cy.get('ion-label[for="register_display_name"')
		cy.get('#register_legal')
		cy.get('ion-label[for="register_legal"')
		cy.get('#submit_button').disabled;
	})
	
	it('can fill an username', () => {
		cy.get('#register_username > input').type('foo');
		cy.get('#register_username > button');
		cy.get('#register_username').parent().get('span').should('have.text', 'Keine gültige E-Mail-Adresse.');
	})
	
	it('can require an username', () => {
		cy.get('#register_username > input').clear();
		cy.get('#register_username_error').should('have.text', 'Dieses Feld darf nicht leer sein.');
	})
	
	it('can validate an username', () => {
		cy.get('#register_username > input').type('test@example.com');
		cy.get('#register_username_error').should('have.text', '');
	})
	
	it('can fill a password', () => {
		cy.get('#register_password > input').type('foo');
		cy.get('#register_password > button');
		cy.get('#register_password_error').should('have.text', 'Min. 8 Zeichen, je ein Groß- und Kleinbuchstaben und eine Zahl.');
	})
	
	it('can validate a password', () => {
		cy.get('#register_password > input').type('ABCabc123');
		cy.get('#register_password > button');
		cy.get('#register_password_error').should('have.text', '');
	})
	
	it('can fill a password', () => {
		cy.get('#register_password_repeat > input').type('foo');
		cy.get('#register_password_repeat > button');
		cy.get('#register_password_repeat_error').should('have.text', 'Die Passwörter stimmen nicht überein.');
	})
	
	it('can validate a confirm password', () => {
		cy.get('#register_password_repeat > input').type('ABCabc123');
		cy.get('#register_password_repeat > button');
		cy.get('#register_password_repeat_error').should('have.text', '');
	})
	
	it('can fill a display name', () => {
		cy.get('#register_display_name > input').type('foo');
		cy.get('#register_display_name > button');
	})
	
	it('can clear a display name', () => {
		cy.get('#register_display_name > button').click();
		cy.get('#register_display_name > input').should('have.value', '');
	})
	
	it('can check the legal button', () => {
		cy.get('#register_legal').click();
		cy.get('#submit_button').not.disabled;
	})
	
	it('can uncheck the legal button', () => {
		cy.get('#register_legal').click();
		cy.get('#register_legal_error').should('have.text', 'Die Hinweise müssen akzeptiert werden.');
		cy.get('#submit_button').disabled;
	})
	
	it('can submit the form, but username is a duplicate', () => {
		cy.get('#register_legal').click();
		cy.get('#register_legal_error').should('have.text', '');
		
		cy.intercept('PUT', '**/auth/signup', {
			statusCode: 409
		})
		cy.get('#submit_button').click();
		cy.get('ion-toast').shadow().find('div.toast-message').contains('Es scheint, als gäbe es');
		
		cy.url().should('include', '/tabs/signup');
		cy.wait(2500);
	})
	
	it('can submit the form, but backend is broken', () => {
		cy.intercept('PUT', '**/auth/signup', {
			statusCode: 500
		})
		cy.get('#submit_button').click();
		cy.get('ion-toast').shadow().find('div.toast-message').contains('Die Registrierung konnte nicht abgeschlossen werden.');
		
		cy.url().should('include', '/tabs/signup');
		cy.wait(2500);
	})
	
	it('can submit the form successful', () => {
		cy.intercept('PUT', '**/auth/signup', {
			statusCode: 201
		})
		cy.get('#submit_button').click();
		cy.get('ion-toast').shadow().find('div.toast-message').contains('Die Registrierung war erfolgreich.');
		
		cy.url().should('include', '/tabs/login');
	})
})
