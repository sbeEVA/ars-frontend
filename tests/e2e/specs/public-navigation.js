describe('Public User', () => {
	it('visits the app root url', () => {
		cy.visit('/')
		cy.url().should('include', '/tabs/join')
		cy.contains('h1', 'Teilnahme');
		cy.get('#tab-button-join')
		cy.get('#tab-button-login')
	})
	
	it('can open the login page', () => {
		cy.get('#tab-button-login').click();
		cy.url().should('include', '/tabs/login')
	})
	
	it('can navigate with back button', () => {
		cy.go('back')
		cy.url().should('include', '/tabs/join');
	})
	
	it('can open the signup page', () => {
		cy.visit('/tabs/login');
		cy.get('#link_signup').click({force: true})
		cy.url().should('include', '/tabs/signup')
	})
})