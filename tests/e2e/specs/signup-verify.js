describe('Sign up', () => {
	it('redirects on invalid token', () => {
		cy.intercept('GET', '**/auth/verify/validationCode', {
			statusCode: 401
		})
		
		cy.visit('/tabs/confirm/validationCode');
		cy.url().should('include', '/tabs/confirm/validationCode');
		cy.get('ion-toast').shadow().find('div.toast-message').contains('Der eingegebene Verifizierungscode ist nicht bekannt.');
		
		cy.wait(2500);
	})
	
	it('redirects on already validated token', () => {
		cy.intercept('GET', '**/auth/verify/validationCode', {
			statusCode: 409
		})
		
		cy.visit('/tabs/confirm/validationCode');
		cy.url().should('include', '/tabs/confirm/validationCode');
		cy.get('ion-toast').shadow().find('div.toast-message').contains('Der Account wurde bereits verifiziert.');
		
		cy.wait(2500);
	})
	
	it('redirects when backend is gone', () => {
		cy.intercept('GET', '**/auth/verify/validationCode', {
			statusCode: 500
		})
		
		cy.visit('/tabs/confirm/validationCode');
		cy.url().should('include', '/tabs/confirm/validationCode');
		cy.get('ion-toast').shadow().find('div.toast-message').contains('Die Verifizierung konnte nicht abgeschlossen werden.');
		
		cy.wait(2500);
	})
	
	it('can show the page on valid verification', () => {
		cy.intercept('GET', '**/auth/verify/validationCode', {
			statusCode: 200
		})
		
		cy.visit('/tabs/confirm/validationCode');
		cy.contains('h1', 'Registrierung verifizieren');
		cy.get('#tab-button-join')
		cy.get('#tab-button-login')
		
		cy.contains('Ihre E-Mail-Adresse wurde erfolgreich registriert.')
		
		cy.url().should('include', '/tabs/confirm/validationCode');
		
		cy.get('ion-grid').find('p > a').click();
		cy.url().should('include', '/tabs/login');
	})
	
})