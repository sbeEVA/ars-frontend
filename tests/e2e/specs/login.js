describe('Login', () => {
	it('contains login form', () => {
		cy.visit('/tabs/login');
		cy.contains('h1', 'Login');
		cy.get('#tab-button-join')
		cy.get('#tab-button-login')
		
		cy.get('#login_username')
		cy.get('ion-label[for="login_username"')
		cy.get('#login_password')
		cy.get('ion-label[for="login_password"')
		cy.get('#submit_button').disabled;
		
		cy.get('#link_signup').not.disabled;
	})
	
	it('can fill the username', () => {
		cy.get('#login_username > input').type('test@example.com');
		cy.get('#login_username > button');
		
		cy.get('#submit_button').disabled;
	})
	
	it('can fill the password', () => {
		cy.get('#login_password > input').type('ABCabc123');
		cy.get('#login_password > button');
		
		cy.get('#submit_button').not.disabled;
	})
	
	it('can login, but invalid username', () => {
		cy.intercept('POST', '**/auth/login', {
			statusCode: 401,
			body: {
				message: 'User not found.'
			}
		})

		cy.get('#submit_button').click();

		cy.get('ion-toast').shadow().find('div.toast-message').contains('Die Kombination aus Nutzernamen und Passwort ist nicht bekannt.');
		cy.wait(2500);
	})
	
	it('can login, but invalid password', () => {
		cy.intercept('POST', '**/auth/login', {
			statusCode: 401,
			body: {
				message: 'Password incorrect.'
			}
		})

		cy.get('#submit_button').click();

		cy.get('ion-toast').shadow().find('div.toast-message').contains('Die Kombination aus Nutzernamen und Passwort ist nicht bekannt.');
		cy.wait(2500);
	})

	it('can login, but users not validated yet', () => {
		cy.intercept('POST', '**/auth/login', {
			statusCode: 401,
			body: {
				message: 'User not verified.'
			}
		})

		cy.get('#submit_button').click();

		cy.get('ion-toast').shadow().find('div.toast-message').contains('Die E-Mail-Adresse wurde noch nicht verifiziert.');
		cy.wait(2500);
	})
	//
	it('can login, but backend is gone', () => {
		cy.intercept('POST', '**/auth/login', {
			statusCode: 500,
		})

		cy.get('#submit_button').click();

		cy.get('ion-toast').shadow().find('div.toast-message').contains('Der Login konnte nicht erfolgen.');
		cy.wait(2500);
	})

	it('can login', () => {
		cy.intercept('POST', '**/auth/login', {
			statusCode: 200
		})

		cy.intercept('GET', '**/user', {
			statusCode: 200,
			body: {
				user: {
					displayName: "Cynthia Press",
					email: "test@example.com",
					createdAt: "2021-12-02T11:08:30.767Z",
					updatedAt: "2021-12-14T07:34:25.259Z",
					language: "de-DE"
				}
			}
		})

		cy.intercept('GET', '**/session', {
			statusCode: 200,
			body: {
				sessions: []
			}
		})

		cy.get('#submit_button').click();
		cy.get('ion-toast').shadow().find('div.toast-message').contains('Sie wurden erfolgreich angemeldet.');

		cy.url().should('include', '/user/session');
	})
})