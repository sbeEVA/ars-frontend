import { createStore } from 'vuex'

import config from '../config';
import userStore from './user';
import participantStore from './participant';
import {Storage} from "@capacitor/storage";
import {Socket} from "socket.io-client";
import socketConnection from "@/api/socketConnection";

export default createStore({
	plugins: [],
	modules: {
		user: userStore,
		participant: participantStore,
	},
	state() {
		return {
			config: config,
			socket: null
		}
	},
	getters: {
		apiUrl: (state: any) => {
			return state.config.API_URL;
		},
		baseUrl: (state: any) => {
			return state.config.BASE_URL;
		},
		socket: (state: any) => {
			return state.socket;
		}
	},
	mutations: {
		setSocket(state: any, socket: Socket|null) {
			state.socket = socket;
		}
	},
	actions: {
		async loadFromStorage(context: any) {
			const token = await Storage.get({key: 'token'});
			if(token.value !== null) {
				await context.dispatch('loginUser', {token: token.value});
			}
			const pToken = await Storage.get({key: 'pToken'});
			if(pToken.value !== null) {
				await context.dispatch('joinSession', {token: pToken.value});
			}
		},
		createSocket(context: any) {
			if(context.getters.socket === null) {
				const socket = socketConnection(context.getters.apiUrl);
				let token = context.getters.token;
				if(token === null) {
					token = context.getters.participateToken;
				}
				const instance = socket.createSocket(token);
				context.commit('setSocket', instance);
			}
		},
		closeSocket(context: any) {
			if(context.getters.socket) {
				context.getters.socket.disconnect();
				context.commit('setSocket', null);
			}
		},
		reconnectSocket(context: any) {
			context.dispatch('closeSocket').then(() => context.dispatch('createSocket'));
		}
	}
});