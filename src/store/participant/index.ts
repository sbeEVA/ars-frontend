import {GetResult, Storage} from "@capacitor/storage";
import store from "@/store";
import participateAPI from "@/api/participate";

export default {
	state: {
		participateToken: null,
		participateSession: null,
	},
	getters: {
		isInSession: (state: any) => {
			return state.participateToken !== null;
		},
		participateToken: (state: any) => {
			return state.participateToken;
		},
		participateSession: (state: any) => {
			return state.participateSession;
		},
	},
	mutations: {
		setParticipateToken(state: any, tokenId: string|null) {
			state.participateToken = tokenId;
		},
		setParticipateSession(state: any, session: any) {
			state.participateSession = session;
		},
		clearForSessionLeave(state: any) {
			state.participateToken = null;
			state.participateSession = null;
		}
	},
	actions: {
		async joinSession(context: any, payload: { token: string }) {
			return new Promise((resolve, reject) => {
				context.commit('setParticipateToken', payload.token);
				Storage.set({key: 'pToken', value: payload.token})
					.then(() => context.dispatch('createSocket'))
					.then(() => {
						const socket = context.getters.socket;
						socket.on('session', (data: any) => {
							if(data.action === 'changed'
								&& context.getters.participateSession
								&& data.session._id === context.getters.participateSession._id) {
								context.commit('setParticipateSession', data.session);
							} else if (data.action === 'closed'
								&& context.getters.participateSession
								&& data.sessionId === context.getters.participateSession._id) {
								context.dispatch('leaveSession');
							}
						});
						resolve(true)
					})
					.catch((err) => {
						console.error(err);
						reject(err.message)
					});
				
			})
			
		},
		async leaveSession(context: any) {
			return new Promise((resolve, reject) => {
				context.commit('clearForSessionLeave');
				Storage.remove({key: 'pToken'})
					.then(() => Storage.remove({key: 'answered'}))
					.then(() => context.dispatch('closeSocket'))
					.then(() => resolve(true))
					.catch((err) => {
						console.error(err);
						reject(err.message)
					})
			})
		},
		async loadParticipateSession(context: any) {
			return new Promise((resolve, reject) => {
				if(context.getters.participateToken) {
					const api = participateAPI(context.getters.apiUrl, context.getters.participateToken);
					api.getSession()
						.then((session) => {
							context.commit('setParticipateSession', session);
							resolve(true);
						})
						.catch((err) => reject(err.message))
				} else {
					reject('User does not participate to a session.');
				}
			});
		},
		async prepareParticipation(context: any, params: { code: string }) {
			return new Promise((resolve, reject) => {
				if(!context.getters.participateToken) {
					const api = participateAPI(context.getters.apiUrl, '');
					api.getSessionIdForAccessCode(params.code)
						.then(result => api.joinSession(result.sessionId))
						.then(result => context.dispatch('joinSession', {token: result.token}))
						.then(() => context.dispatch('loadParticipateSession'))
						.then(() => resolve(true))
						.catch((err) => {
							console.error(err);
							reject(err.message);
						})
				} else if (!store.getters.participateSession) {
					context.dispatch('loadParticipateSession')
						.then(() => resolve(true))
						.catch((err: any) => reject(err.message));
				} else {
					resolve(true);
				}
			})
		},
		async saveParticipationOnQuestion(context: any, params: { questionId: string }) {
			return new Promise((resolve, reject) => {
				Storage.get({key: 'answered'})
					.then((existing: GetResult) => {
						let newValues;
						if(existing.value === null) {
							newValues = [params.questionId];
						} else {
							newValues = JSON.parse(existing.value);
							if(Array.isArray(newValues)) {
								newValues.push(params.questionId);
							} else {
								newValues = [params.questionId];
							}
							
						}
						return Storage.set({key: 'answered', value: JSON.stringify(newValues)});
					})
					.then(() => resolve(true))
					.catch(() => reject('Could not update answered store.'));
				
			})
		},
		async getParticipatedQuestions() {
			return new Promise((resolve, reject) => {
				Storage.get({key: 'answered'})
					.then((existing: GetResult) => {
						if(existing.value === null) {
							resolve([]);
						} else {
							resolve(JSON.parse(existing.value));
						}
					})
					.catch(() => reject('Could not get answered store.'));
			})
		}
	}
}