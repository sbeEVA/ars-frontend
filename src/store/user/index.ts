import {Storage} from '@capacitor/storage';
import {Session} from "@/model/Session";
import authAPI from "@/api/auth";
import sessionAPI from "@/api/session";

export default {
	state: {
		token: null,
		userDetails: null,
		userSessions: null
	},
	getters: {
		isAuthenticated: (state: any) => {
			return state.token !== null;
		},
		token: (state: any) => {
			return state.token;
		},
		userDetails: (state: any) => {
			return state.userDetails;
		},
		userName: (state: any) => {
			return state.userDetails === null ? null : state.userDetails.displayName;
		},
		userLanguage: (state: any) => {
			return state.userDetails === null ? 'de-DE' : state.userDetails.language;
		},
		userSessions: (state: any) => {
			return state.userSessions;
		},
		userSession: (state: any) => {
			return (id: string) => {
				if(state.userSessions !== null) {
					return state.userSessions.find((element: any) => element._id === id)
				}
				return null;
			}
		}
	},
	mutations: {
		setToken(state: any, tokenId: string|null) {
			state.token = tokenId;
		},
		setUserDetails(state: any, userDetails: any) {
			state.userDetails = userDetails;
		},
		setUserSessions(state: any, sessions: Session[]) {
			state.userSessions = null; //reset first
			if(sessions.length > 0) {
				state.userSessions = [];
				sessions.forEach(session => {
					session.createdAt = new Date(session.createdAt);
					session.updatedAt = new Date(session.updatedAt);
					
					state.userSessions.push(session);
				});
			}
		},
		addUserSession(state: any, session: Session) {
			if(state.userSessions === null) {
				state.userSessions = [];
			}
			
			session.createdAt = new Date(session.createdAt);
			session.updatedAt = new Date(session.updatedAt);
			
			//add or overwrite
			const existing = state.userSessions.findIndex((element: any) => element._id === session._id);
			if(existing >= 0) {
				state.userSessions[existing] = session;
			} else {
				state.userSessions.push(session);
			}
		},
		removeUserSession(state: any, id: string) {
			if(state.userSessions !== null) {
				const existing = state.userSessions.findIndex((element: any) => element._id === id);
				if(existing >= 0) {
					state.userSessions.splice(existing, 1);
				}
			}
		},
		clearForLogout(state: any) {
			state.token = null;
			state.userDetails = null;
			state.userSessions = null;
		}
	},
	actions: {
		async loginUser(context: any, payload: any) {
			return new Promise((resolve, reject) => {
				context.commit('setToken', payload.token);
				Storage.set({key: 'token', value: payload.token})
					.then(() => context.dispatch('createSocket'))
					.then(() => {
						const socket = context.getters.socket;
						socket.on('session', (data: any) => {
							if(data.action === 'changed') {
								context.commit('addUserSession', data.session);
							}
						});
						resolve(true)
					})
					.catch((err) => reject(err.message))
			})
		},
		async logoutUser(context: any) {
			return new Promise((resolve, reject) => {
				context.commit('clearForLogout');
				Storage.remove({key: 'token'})
					.then(() => context.dispatch('closeSocket'))
					.then(() => resolve(true))
					.catch((err) => reject(err.message))
			})
		},
		async loadUserDetails(context: any) {
			return new Promise((resolve, reject) => {
				if(context.getters.userDetails === null) {
					const {getUserDetails} = authAPI(context.getters.apiUrl, context.getters.token);
					getUserDetails().then((user) => {
						context.commit('setUserDetails', user);
						resolve(true);
					}).catch((err) => reject(err.message));
					return;
				}
				resolve(true);
			});
		},
		async loadUserSessions(context: any, payload: {reload: boolean}) {
			return new Promise((resolve, reject) => {
				if(context.getters.userSessions === null || (payload && payload.reload)) {
					const {getSessionsOfUser} = sessionAPI(context.getters.apiUrl, context.getters.token);
					getSessionsOfUser()
						.then(sessions => {
							context.commit('setUserSessions', sessions)
							resolve(true);
						}).catch(err => {
							if(err.message === 'invalidUser') {
								context.dispatch('logoutUser');
							}
							reject(err.message);
						})
					return;
				}
				resolve(true);
			})
		
		},
		async addUserSession(context: any, payload: { sessionId: string}) {
			return new Promise((resolve, reject) => {
				if(context.getters.userSessions === null) {
					//load all
					context.dispatch('loadUserSessions').then((result: boolean) => {
						result ? resolve(true) : reject('Could not load user sessions.');
					}).catch((err: any) => reject(err.message));
					return;
				}
				
				const {getSessionById} = sessionAPI(context.getters.apiUrl, context.getters.token);
				getSessionById(payload.sessionId).then((newSession: Session|null) => {
					//replace
					context.commit('removeUserSession', payload.sessionId);
					if(newSession !== null) {
						context.commit('addUserSession', newSession);
					}
					resolve(true);
				}).catch(err => {
					if(err.message === 'invalidUser') {
						context.dispatch('logoutUser');
					}
					reject(err.message);
				})
			})
			
		}
	}
}