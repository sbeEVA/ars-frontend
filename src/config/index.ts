let env = 'development';

if(['production', 'development'].includes(process.env.NODE_ENV)){
	env = process.env.NODE_ENV;
}
export default require('./'+env+'.json');
