import {useStore} from "vuex";
import useToastMessage from "@/composables/toastMessage";
import authAPI from "@/api/auth";

export default function useAuthAPI() {
	const store = useStore();
	
	const { openToast } = useToastMessage();
	const api = authAPI(store.getters.apiUrl, store.getters.token);
	
	const signup = async (data: { username: string, email: string, password: string, displayName: string }): Promise<boolean> => {
		return api.signup(data).then(async () => {
			await openToast('registration.success', 4000);
			return true;
		}).catch(async (err) => {
			await openToast(err.message);
			return false;
		});
	}
	
	const validateEmail = async(code: string): Promise<boolean> => {
		return api.validateEmail(code).then(async () => {
			await openToast('registration.verify.success', 4000);
			return true;
		}).catch(async (err) => {
			await openToast(err.message);
			return false;
		})
	}
	
	const login = async(username: string, password: string, keepLoggedIn = false): Promise<boolean> => {
		return api.login(username, password, keepLoggedIn).then(async (userData) => {
			await openToast('login.success');
			await store.dispatch('loginUser', userData);
			await store.dispatch('loadUserDetails');
			return true;
		}).catch(async (err) => {
			await openToast(err.message);
			return false;
		})
	}
	
	const getUserDetails = async (): Promise<any|null> => {
		return api.getUserDetails()
			.then(response => response)
			.catch(async() => {
				await store.dispatch('logoutUser');
				return null;
			});
	}
	
	return {signup, validateEmail, login, getUserDetails};
	
}