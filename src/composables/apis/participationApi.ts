import {useStore} from "vuex";
import useToastMessage from "@/composables/toastMessage";
import participateAPI from "@/api/participate";

export default function useParticipationAPI() {
	const store = useStore();
	
	const api = participateAPI(store.getters.apiUrl, store.getters.participateToken);
	
	const { openToast } = useToastMessage();
	
	/**
	 * Checks if the session exists. If it does and is valid, the user will be added to this session
	 * @param code	AccessCode of the Session
	 * @return <boolean> Returns true if it was successful
	 */
	const getSessionIdForAccessCode = async (code: string): Promise<string|null> => {
		return api.getSessionIdForAccessCode(code)
			.then(async (result) => {
				if(result.open) {
					return result.sessionId;
				}
				return null;
			})
			.catch(err => {
				openToast(err.message);
				return null;
			});
	}
	
	/**
	 * Join the session as participant by sessionId
	 * @param sessionId
	 * @return <boolean> Returns true if it was successful
	 */
	const joinSession = async (sessionId: string): Promise<boolean> => {
		return api.joinSession(sessionId)
			.then(async (result) => {
				await store.dispatch('joinSession', {token: result.token});
				return true;
			})
			.catch(err => {
				openToast(err.message);
				return false;
			});
	}
	
	const sendAnswer = async (data: {questionId: string, answer: number[]|number}) => {
		return api.sendAnswer(data)
			.then(async () => {
				await openToast('participate.question.success')
				await store.dispatch('saveParticipationOnQuestion', {questionId: data.questionId});
				return true;
			})
			.catch((err) => {
				openToast(err.message);
				return false;
			})
	}
	
	return {getSessionIdForAccessCode, joinSession, sendAnswer};
	
}