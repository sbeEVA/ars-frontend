import {useStore} from "vuex";
import useToastMessage from "@/composables/toastMessage";
import questionAPI from "@/api/question";

export default function useQuestionAPI() {
	const store = useStore();
	
	const api = questionAPI(store.getters.apiUrl, store.getters.token);
	
	const { openToast } = useToastMessage();
	
	const getQuestionById = async (sessionId: string, id: string): Promise<any|null> => {
		return api.getQuestionById(sessionId, id)
			.then(question => question)
			.catch(async (err) => {
				if(err.message === 'invalidUser') {
					await store.dispatch('logoutUser');
				}
				return null;
			});
	}
	
	const insertQuestion = async (data: any, sessionId: string ): Promise<string|null> => {
		return api.insertQuestion(data, sessionId)
			.then(async(questionId) => {
				await openToast('questions.add.success');
				return questionId;
			})
			.catch(async (err) => {
				if(err.message === 'invalidUser') {
					await store.dispatch('logoutUser');
				} else {
					await openToast('questions.add.failure');
				}
				return null;
			});
	};
	
	const updateQuestion = async (data: any, questionId: string, sessionId: string, silent = false ): Promise<boolean> => {
		return api.updateQuestion(sessionId, questionId, data)
			.then(async () => {
				if(!silent){
					await openToast('questions.edit.success');
				}
				return true;
			})
			.catch(async (err) => {
				if(err.message === 'invalidUser') {
					await store.dispatch('logoutUser');
				} else {
					if(!silent){
						await openToast('questions.edit.failure');
					}
				}
				return false;
			});
	};
	
	const deleteQuestion = async (sessionId: string, id: string): Promise<boolean> => {
		return api.deleteQuestion(sessionId, id)
			.then(async () => {
				await openToast('questions.delete.success');
				return true;
			})
			.catch(async (err: any) => {
				if(err.message === 'invalidUser') {
					await store.dispatch('logoutUser');
				} else {
					await openToast(err.message);
				}
				return false;
			});
	}
	
	return {insertQuestion, updateQuestion, getQuestionById, deleteQuestion};
}