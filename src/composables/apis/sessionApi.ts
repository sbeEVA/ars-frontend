import {useStore} from "vuex";
import useToastMessage from "@/composables/toastMessage";
import {Session} from '@/model/Session';
import sessionAPI from "@/api/session";

export default function useSessionAPI() {
	const store = useStore();
	
	const api = sessionAPI(store.getters.apiUrl, store.getters.token);
	
	const { openToast } = useToastMessage();
	
	const getSessionsOfUser = async (): Promise<Session[]> => {
		return api.getSessionsOfUser()
			.then(result => result)
			.catch(async (err) => {
				if(err.message === 'invalidUser') {
					await store.dispatch('logoutUser');
				}
				return [];
			})
	}
	
	const getSessionById = async (id: string): Promise<Session|null> => {
		return api.getSessionById(id)
			.then(session => session)
			.catch(async (err) => {
				if(err.message === 'invalidUser') {
					await store.dispatch('logoutUser');
				}
				return null;
			});
	}
	
	const insertSession = async (data: any ): Promise<string|null> => {
		return api.insertSession(data)
			.then(async (sessionId: string) => {
				await openToast('sessions.add.success');
				return sessionId;
			})
			.catch(async (err) => {
				if(err.message === 'invalidUser') {
					await store.dispatch('logoutUser');
				} else {
					await openToast(err.message);
				}
				return null;
			});
	};
	
	const updateSession = async (id: string, data: any ): Promise<boolean> => {
		return api.updateSession(id, data)
			.then(() => true)
			.catch(async (err) => {
				if(err.message === 'invalidUser') {
					await store.dispatch('logoutUser');
				} else {
					await openToast(err.message);
				}
				return false;
			});
	}
	
	const deleteSession = async (id: string): Promise<boolean> => {
		return api.deleteSession(id)
			.then(async () => {
				await openToast('sessions.edit.deleteSuccess');
				return true;
			})
			.catch(async (err: any) => {
				if(err.message === 'invalidUser') {
					await store.dispatch('logoutUser');
				} else {
					await openToast(err.message);
				}
				return false;
			});
	}
	
	return {getSessionsOfUser, insertSession, getSessionById, updateSession, deleteSession};
	
}