import {computed, watchEffect} from "vue";
import {useStore} from "vuex";
import useToastMessage from "@/composables/toastMessage";
import {useIonRouter} from "@ionic/vue";

export default function useParticipantWatcher() {
	const store = useStore();
	const router = useIonRouter();
	
	const {openToast} = useToastMessage();
	
	const token = computed(() => {
		return store.getters.participateToken;
	})
	
	const stopTokenWatcher = watchEffect(() => {
		if(!token.value) {
			openToast('participate.sessionHasClosed', 5000).then(() => {
				// router.replace({name: 'join'});
				router.push({name: 'join'});
			});
		}
	});
	
	return {stopTokenWatcher};
}