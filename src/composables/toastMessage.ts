import {toastController} from "@ionic/vue";
import {useI18n} from "vue-i18n";

export default function useToastMessage() {
	const {t} = useI18n();
	
	const openToast = async function (messageCode: string, duration = 2000) {
		const toast = await toastController
			.create({
				message: t(messageCode),
				duration: duration
			})
		return toast.present();
	};
	
	return {openToast};
}