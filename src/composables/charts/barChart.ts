import {computed, ComputedRef, ref} from "vue";
import Chart, {TooltipItem} from "chart.js/auto";

export default function useBarChart(question: any, indexAxis: "x" | "y" = "x" ) {
	const answers = computed(() => question.value ? question.value.question.answers: []);
	
	const chart = ref();
	const ctx = ref();
	
	const answerOptions = computed(() => {
		const labels: string[] = [];
		
		answers.value.forEach((answer: any) => {
			labels.push(answer.text);
		})
		
		return labels;
	})
	
	const createGraph = (canvasId: ComputedRef, results: ComputedRef) => {
		const canvas: any = document.getElementById(canvasId.value);
		if(!canvas) {
			return;
		}
		if(ctx.value == null) {
			ctx.value = canvas.getContext('2d');
		}
		
		if(chart.value != null) {
			chart.value.destroy();
		}
		chart.value = new Chart(ctx.value, {
			type: 'bar',
			data: {
				labels: answerOptions.value,
				datasets: [{
					label: '',
					data: results.value,
					backgroundColor: [
						'rgb(9, 90, 90)',
					],
					borderWidth: 0
				}]
			},
			options: {
				indexAxis: indexAxis,
				scales: { y: { beginAtZero: true }	},
				plugins: {
					legend: {
						display: false,
						position: 'bottom'
					},
					tooltip: {
						callbacks: {
							label: function(context: TooltipItem<"bar">) {
								return "" + (indexAxis === 'x' ? context.parsed.y : context.parsed.x);
							}
						}
					}
				},
			},
		});
	}
	
	return {
		chart, ctx, createGraph
	}
}