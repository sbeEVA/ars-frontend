import {computed, onMounted, watch, ComputedRef} from "vue";
import useBarChart from "@/composables/charts/barChart";

export default function useSingleChoiceChart(session: ComputedRef, index: ComputedRef, canvasId: ComputedRef) {
	const question = computed(() => session.value && session.value.questions ? session.value.questions[index.value] : null);
	const {chart, createGraph} = useBarChart(question);
	
	const answers = computed(() => question.value ? question.value.question.answers: []);
	const results = computed(() => question.value ? question.value.results: []);
	
	const groupedResults = computed(() => {
		const counts: any = {};
		answers.value.forEach((answer: any) => {
			counts[answer.value] = 0;
		})
		
		for (const num of results.value) {
			counts[num] = counts[num] ? counts[num] + 1 : 1;
		}
		
		return Object.values(counts);
	})
	
	onMounted(() => {
		createGraph(canvasId, groupedResults);
	});
	
	watch(results, (resultsNew, resultsOld) => {
		if(resultsNew.length !== resultsOld.length) {
			createGraph(canvasId, groupedResults);
		}
	});
	
	
	return {
		chart
	}
}