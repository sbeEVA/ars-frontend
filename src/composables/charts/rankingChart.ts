import {computed, onMounted, watch, ComputedRef} from "vue";
import useBarChart from "@/composables/charts/barChart";

export default function useRankingChart(session: ComputedRef, index: ComputedRef, canvasId: ComputedRef) {
	const question = computed(() => session.value && session.value.questions ? session.value.questions[index.value] : null);
	const {chart, createGraph} = useBarChart(question, "y");
	
	const answers = computed(() => question.value ? question.value.question.answers: []);
	const results = computed(() => question.value ? question.value.results: []);
	
	const groupedResults = computed(() => {
		const counts: any = {};
		answers.value.forEach((answer: any) => {
			counts[answer.value] = 0;
		})
		
		const resultValues = [...results.value];
		for (const index in  resultValues) {
			let answer = resultValues[index];
			//here we got the array in the order of the answers:
			for(let i = 0, iMax = answer.length; i < iMax; i++) {
				const temp = Array(iMax - i - 1).fill(answer[i]);
				answer = answer.concat(temp);
			}
			resultValues[index] = answer;
		}
		for (const num of resultValues.flat()) {
			counts[num] = counts[num] ? counts[num] + 1 : 1;
		}
		
		for (const [key, value] of Object.entries(counts)) {
			counts[key] = parseInt(String(value)) / resultValues.length;
		}
		
		return Object.values(counts);
	})
	
	onMounted(() => {
		createGraph(canvasId, groupedResults);
	});
	
	watch(results, (resultsNew, resultsOld) => {
		if(resultsNew.length !== resultsOld.length) {
			createGraph(canvasId, groupedResults);
		}
	});
	
	return {
		chart
	}
}