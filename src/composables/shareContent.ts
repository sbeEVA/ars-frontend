import {computed} from "vue";
import {isPlatform} from "@ionic/vue";
import {Share} from "@capacitor/share";
import {Clipboard} from "@capacitor/clipboard";
import {
	clipboard,
	clipboardOutline,
	clipboardSharp,
	shareSocial,
	shareSocialOutline,
	shareSocialSharp
} from "ionicons/icons";
import useToastMessage from "@/composables/toastMessage";
import {useI18n} from "vue-i18n";

export default function useShareContent() {
	const {openToast} = useToastMessage();
	const { t } = useI18n();
	
	const canShare = computed(() => {
		return isPlatform('ios') || isPlatform('android');
	})
	
	const shareContent = async (content: any, options: {additionalText: string}|null = null) => {
		if(canShare.value) {
			content.title = t(content.title);
			if(options && options.additionalText) {
				content.text = t(options.additionalText, {text: content.text});
			} else {
				content.text = t(content.text);
			}
			await Share.share(content);
		} else if( navigator.clipboard) {
			await Clipboard.write({
				string: content.url ? content.url : content.text
			});
			await openToast('sessions.edit.copied');
		} else {
			await openToast('sessions.edit.noClipboard');
		}
	}
	
	const shareIcons = canShare.value
		? {share: shareSocial, shareOutline: shareSocialOutline, shareSharp: shareSocialSharp}
		: {share: clipboard, shareOutline: clipboardOutline, shareSharp: clipboardSharp}
	
	return {
		...shareIcons, canShare, shareContent
	}
}