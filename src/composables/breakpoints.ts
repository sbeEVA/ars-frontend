/**
 * Credits to https://stackoverflow.com/questions/47219272/how-can-i-use-window-size-in-vue-how-do-i-detect-the-soft-keyboard
 * Adapted for the ionic grid sizes
 */
import {computed, onMounted, onUnmounted, ref} from "vue"
import {isPlatform} from "@ionic/vue";

export default function useBreakpoints() {
	const windowWidth = ref(window.innerWidth)
	
	const onWidthChange = () => windowWidth.value = window.innerWidth
	onMounted(() => window.addEventListener('resize', onWidthChange))
	onUnmounted(() => window.removeEventListener('resize', onWidthChange))
	
	const type = computed(() => {
		if(windowWidth.value < 576) return 'xs';
		if(windowWidth.value < 768) return 'sm';
		if(windowWidth.value < 992) return 'md';
		if(windowWidth.value < 1200) return 'lg';
		return 'xl';
	})
	
	const width = computed(() => windowWidth.value);
	
	const isApp = computed(() => {
		return isPlatform('hybrid') || ['xs', 'sm'].includes(type.value);
	})
	
	const menuPos = computed(() => {
		return isApp.value ? 'bottom' : 'top';
	})
	
	return {width, type, menuPos, isApp}
}
