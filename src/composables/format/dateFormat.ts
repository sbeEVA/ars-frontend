export default function useDateFormatter() {
	
	return {
		formatDate: (date: Date, locale= 'de-DE') => {
			return date.toLocaleString(locale, {
				year: 'numeric',
				month: 'long',
				day: 'numeric',
				hour: '2-digit',
				minute: '2-digit'
			});
		}
	}
}