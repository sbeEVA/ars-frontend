import {useI18n} from "vue-i18n";
import {useField, useForm} from "vee-validate";
import * as yup from "yup";
import {computed} from "vue";

export default function useQuestionEditForm(initQuestion: any) {
	const { t } = useI18n();
	
	const {meta: formMeta} = useForm({
		validationSchema: yup.object({
			text: yup.string().required(t('validation.required')),
			type: yup.string().matches(/(S|SC|MC|RK)/, t('validation.invalidQuestionType')),
			answers: yup.array().min(2, t('validation.tooFewQuestionAnswers')),
			isOpen: yup.boolean().notRequired(),
			shareResults: yup.boolean().notRequired(),
		}),
		initialValues: {
			text: initQuestion.question.text,
			type: initQuestion.question.type,
			answers: initQuestion.question.answers,
			isOpen: initQuestion.isOpen,
			shareResults: initQuestion.shareResults
		}
	});
	
	const { value: questionText, errorMessage: questionTextError } = useField('text');
	const { value: questionType, errorMessage: questionTypeError } = useField('type');
	const { value: questionAnswers, errorMessage: questionAnswersError } = useField('answers');
	const { value: questionIsOpen } = useField('isOpen');
	const { value: questionShareResults } = useField('shareResults');
	
	const validForm = computed(() => {
		return formMeta.value.valid && formMeta.value.dirty;
	})
	
	return {
		questionText, questionTextError,
		questionType, questionTypeError,
		questionAnswers, questionAnswersError,
		questionIsOpen,
		questionShareResults,
		validForm
	}
}