import {useI18n} from "vue-i18n";
import {setLocale} from "yup";
import {useField, useForm} from "vee-validate";
import * as yup from "yup";
import {computed} from "vue";

export default function useRegistrationForm() {
	const { t } = useI18n();
	setLocale({
		mixed: { required: t('validation.required') },
	});
	const {meta: formMeta} = useForm({
		validationSchema: yup.object({
			username: yup.string().required().email(t('validation.noEmail')),
			displayName: yup.mixed().notRequired(),
			password: yup.string().required().matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/, t('validation.passwordRule')),
			passwordRepeat: yup.string().required().oneOf([yup.ref('password'), null], t('validation.passwordMatch')),
			acceptLegal: yup.boolean().required(t('validation.acceptLegal')).oneOf([true], t('validation.acceptLegal'))
		}),
	});
	
	const { value: username, errorMessage: usernameError } = useField('username');
	const { value: password, errorMessage: passwordError } = useField('password');
	const { value: passwordRepeat, errorMessage: passwordRepeatError } = useField('passwordRepeat');
	const { value: acceptLegal, errorMessage: acceptLegalError } = useField('acceptLegal');
	const { value: displayName, errorMessage: displayNameError } = useField('displayName');
	
	const validForm = computed(() => {
		return formMeta.value.valid && formMeta.value.dirty;
	})
	
	return {
		username, displayName, password, passwordRepeat, acceptLegal,
		usernameError, displayNameError, passwordError, passwordRepeatError, acceptLegalError,
		validForm
	}
}