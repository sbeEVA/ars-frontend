import {useI18n} from "vue-i18n";
import {useField, useForm} from "vee-validate";
import * as yup from "yup";
import {computed} from "vue";

export default function useSessionAddForm() {
	const { t } = useI18n();
	
	const {meta: formMeta} = useForm({
		validationSchema: yup.object({
			sessionName: yup.string().required(t('validation.required')),
			startOpen: yup.boolean().notRequired(),
		}),
	});
	
	const { value: sessionName, errorMessage: sessionNameError } = useField('sessionName');
	const { value: startOpen } = useField('startOpen');
	
	const validForm = computed(() => {
		return formMeta.value.valid && formMeta.value.dirty;
	})
	
	return {
		sessionName, sessionNameError, startOpen, validForm
	}
}