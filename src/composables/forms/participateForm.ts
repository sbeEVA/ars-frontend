import {useI18n} from "vue-i18n";
import {useField, useForm} from "vee-validate";
import * as yup from "yup";
import {computed} from "vue";

export default function useParticipateForm() {
	const { t } = useI18n();
	
	const {meta: formMeta} = useForm({
		validationSchema: yup.object({
			code: yup.string().min(8, t('validation.participateCodeFormat'))
							  .max(8, t('validation.participateCodeFormat'))
							  .matches(/^[a-zA-Z\d]*$/, t('validation.participateCodeFormat'))
							  .required(t('validation.required')),
		}),
	});
	
	const { value: code, errorMessage: codeError } = useField('code');

	const validForm = computed(() => {
		return formMeta.value.valid && formMeta.value.dirty;
	})
	
	return {
		code, codeError,
		validForm
	}
}