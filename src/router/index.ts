import {createRouter, createWebHistory} from '@ionic/vue-router';
import {RouteRecordRaw} from 'vue-router';
import PublicTabs from '../views/Tabs/PublicTabs.vue'
import UserTabs from '../views/Tabs/UserTabs.vue'
import store from '../store';
import ParticipationTabs from "@/views/Tabs/ParticipationTabs.vue";

const routes: Array<RouteRecordRaw> = [
	{
		path: '/',
		redirect: '/tabs/join'
	},
	{
		path: '/tabs/',
		name: 'public',
		component: PublicTabs,
		children: [
			{
				path: '',
				redirect: '/tabs/join'
			},
			{
				path: 'join',
				name: 'join',
				component: () => import('@/views/participate/ParticipateView.vue'),
				beforeEnter: (to, from, next) => {
					if(store.getters.participateToken) {
						store.dispatch('prepareParticipation')
							.then(() => {
								next({
									name: 'participateOverview',
									params: {code: store.getters.participateSession.accessCode}
								});
							})
							.catch(async () => {
								await store.dispatch('leaveSession');
								next();
							});
					} else {
						next();
					}
				}
			},
			{
				path: 'login',
				name: 'login',
				component: () => import('@/views/authorization/LoginView.vue')
			},
			{
				path: 'signup',
				name: 'signup',
				component: () => import('@/views/authorization/RegistrationView.vue'),
			},
			{
				path: 'confirm/:code',
				component: () => import('@/views/authorization/RegistrationConfirmationView.vue'),
			},
		]
	},
	{
		path: '/user/',
		name: 'user',
		component: UserTabs,
		children: [
			{
				path: '',
				redirect: '/user/session'
			},
			{
				path: 'session',
				name: 'sessionList',
				component: () => import('@/views/session/SessionListView.vue'),
			},
			{
				path: 'session/:id',
				name: 'sessionEdit',
				component: () => import('@/views/session/SessionEditView.vue')
			},
			{
				path: 'session/add',
				name: 'sessionAdd',
				component: () => import('@/views/session/SessionAddView.vue'),
			},
			{
				path: 'session/:sessionId/add',
				name: 'questionAdd',
				component: () => import('@/views/session/QuestionAddView.vue')
			},
			{
				path: 'session/:sessionId/:id',
				name: 'questionEdit',
				component: () => import('@/views/session/QuestionEditView.vue')
			},
			{
				path: 'session/:sessionId/:id/results',
				name: 'questionResults',
				component: () => import('@/views/session/QuestionResultView.vue')
			}
		]
	},
	{
		path: '/participate/',
		name: 'participate',
		component: ParticipationTabs,
		children: [
			{
				path: '',
				redirect: 'participate/join'
			},
			{
				path: 'join',
				redirect: '/tabs/join'
			},
			{
				path: ':code',
				name: 'participateOverview',
				component: () => import('@/views/participate/ParticipateOverviewView.vue'),
				beforeEnter: (to, from, next) => {
					store.dispatch('prepareParticipation', {code: to.params.code})
						.then(() => next())
						.catch(() => next({name: 'join'}))
				}
			},
			{
				path: ':code/:questionId',
				name: 'participateQuestion',
				component: () => import('@/views/participate/ParticipateQuestionView.vue'),
				beforeEnter: (to, from, next) => {
					store.dispatch('prepareParticipation', {code: to.params.code})
						.then(() => next())
						.catch(() => next({name: 'join'}))
				}
			},
			{
				path: ':code/:questionId/results',
				name: 'participateQuestionResults',
				component: () => import('@/views/participate/ParticipateQuestionResultsView.vue'),
				beforeEnter: (to, from, next) => {
					store.dispatch('prepareParticipation', {code: to.params.code})
						.then(() => next())
						.catch(() => next({name: 'join'}))
				}
			}
		]
	}
]

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes
})

router.beforeResolve(async (to, from, next) => {
	if(to.path.startsWith('/tabs/') && store.getters.isAuthenticated) {
		next({name: 'sessionList'});
	}else if(to.path.startsWith('/user/') && !store.getters.isAuthenticated) {
		next({name: 'public'});
	} else {
		next();
	}
})

export default router
