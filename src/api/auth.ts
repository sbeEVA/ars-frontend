import axios from "axios";

/**
 * Handles all the Auth and User relevant API calls.
 * @param apiUrl
 * @param token
 */
export default function authAPI(apiUrl: string, token: string) {
	const _getAuthHeader = () => {
		return {
			headers: {
				authorization: 'Bearer '+ token,
			}
		}
	}
	
	/**
	 * Signs up a user in the backend, if possible. Throws errors if the signup was not successful.
	 * @param data
	 * @throws Error
	 */
	const signup = async (data: { username: string, email: string, password: string, displayName: string }): Promise<boolean> => {
		//add data for email in backend
		const postData = {
			...data,
			username: data.username.toLowerCase(),
			locale: 'de-DE',
			confirmUrl: 'tabs/confirm/'
		};
		
		return axios.put(apiUrl+'/auth/signup', postData)
			.then(async () => {
				return true;
			})
			.catch(async (err) => {
				if(err.request.status === 409) {
					throw new Error('registration.userAlreadyExists');
				}else{
					console.error(err);
					throw new Error('registration.failure');
				}
			});
	}
	
	/**
	 * Validates the confirmation code of the email. Throws error if the validation failed
	 * @param code
	 * @throws Error
	 */
	const validateEmail = async(code: string): Promise<boolean> => {
		return axios.get(apiUrl+'/auth/verify/'+code)
			.then(async () => {
				return true;
			})
			.catch(async (err) => {
				if (err.request.status === 401){
					//token does not exist
					throw new Error('registration.verify.unknownToken');
				} else if(err.request.status === 409) {
					throw new Error('registration.verify.tokenAlreadyActivated');
				} else {
					//unknown error
					console.error(err);
					throw new Error('registration.verify.failure');
				}
			});
	}
	
	/**
	 * Sends a login request to the backend. Throws an error in case of invalid login
	 * @param username
	 * @param password
	 * @param keepLoggedIn
	 * @throws Error
	 */
	const login = async(username: string, password: string, keepLoggedIn = false): Promise<any> => {
		const data = {username: username.toLowerCase(), password: password, keepLoggedIn: keepLoggedIn};
		return axios.post(apiUrl+'/auth/login', data)
			.then(async (response) => {
				return {userId: response.data.userId, token: response.data.token};
			})
			.catch(async (err) => {
				if(err.response && err.response.data) {
					switch(err.response.data.message){
						case 'User not found.':
							throw new Error('login.unknownUser');
						case 'User not verified.':
							throw new Error('login.userNotVerified');
						case 'Password incorrect.':
							throw new Error('login.passwordIncorrect');
						default:
							console.error(err);
							throw new Error('login.failure');
					}
				}else{
					console.error(err);
					throw new Error('login.failure');
				}
			});
	}
	
	const getUserDetails = async (): Promise<any|null> => {
		return await axios.get(apiUrl + '/user', _getAuthHeader())
			.then(async(response) => {
				return response.data.user;
			})
			.catch(async() => {
				return null;
			});
	}
	
	return {signup, validateEmail, login, getUserDetails};
	
}