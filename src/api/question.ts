import axios from "axios";

export default function questionAPI(apiUrl: string, token: string) {
	const _getAuthHeader = () => {
		return {
			headers: {
				authorization: 'Bearer ' + token,
			}
		}
	}
	
	const _isNumeric = (possibleNumericValue: any) => {
		return /^\d+$/.test(possibleNumericValue);
	}
	
	const getQuestionById = async (sessionId: string, id: string): Promise<any|null> => {
		return axios.get(apiUrl + '/session/'+sessionId+'/question/'+id, _getAuthHeader())
			.then(async(response) => {
				return response.data.sessionQuestion;
			})
			.catch(async (err) => {
				if(err.request.status === 401) {
					throw new Error('invalidUser');
				} else {
					console.error(err, err.response);
				}
				return null;
			});
	}
	
	const insertQuestion = async (data: any, sessionId: string ): Promise<string> => {
		//only the texts are necessary for insert
		data.answers = data.answers.map((answer: any) => {
			return {text: answer.text};
		})
		
		return axios.put(apiUrl+'/session/'+sessionId+'/question', data, _getAuthHeader())
			.then(async (result) => {
				return result.data.questionId;
			})
			.catch(async (err) => {
				if(err.request.status === 401) {
					throw new Error('invalidUser');
				} else {
					console.error(err, err.response);
					throw new Error('questions.add.failure');
				}
			});
	};
	
	const updateQuestion = async (sessionId: string, questionId: string, data: any): Promise<boolean> => {
		if(data.answers) {
			data.answers = data.answers.map((answer: any) => {
				if(_isNumeric(answer._id)) { //real ids have chars in it
					return {text: answer.text, _id: answer._id};
				}
				return {text: answer.text};
			})
		}
		
		return axios.patch(apiUrl+'/session/'+sessionId+'/question/'+questionId, data, _getAuthHeader())
			.then(async () => {
				return true;
			})
			.catch(async (err) => {
				if(err.request.status === 401) {
					throw new Error('invalidUser');
				} else {
					console.error(err, err.response);
					throw new Error('questions.edit.failure');
				}
			});
	};
	
	const deleteQuestion = async (sessionId: string, questionId: string): Promise<boolean> => {
		return axios.delete(apiUrl+'/session/'+sessionId+'/question/'+questionId, _getAuthHeader())
			.then(async () => {
				return true;
			})
			.catch(async (err) => {
				if(err.request.status === 401) {
					throw new Error('invalidUser');
				} else {
					console.error(err, err.response);
					throw new Error('questions.delete.failure');
				}
			});
	};
	
	return {insertQuestion, updateQuestion, deleteQuestion, getQuestionById};
}