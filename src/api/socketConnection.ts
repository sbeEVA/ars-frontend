import io from 'socket.io-client';

export default function socketConnection(apiUrl: string) {

	const createSocket = (token: string) => {
		return io(apiUrl, {
			query: {token: token}
		});
	}
	
	return {createSocket}
}