import axios from "axios";
import {Session} from "@/model/Session";

export default function participateAPI(apiUrl: string, token: string) {
	const _getAuthHeader = () => {
		return {
			headers: {
				authorization: 'Bearer ' + token,
			}
		}
	}
	
	const getSessionIdForAccessCode = async (code: string): Promise<any> => {
		return axios.get(apiUrl + '/participate/'+code.toUpperCase()+'/exist')
			.then((response) => {
				return  {open: response.data.open, sessionId: response.data.sessionId};
			})
			.catch(async (err) => {
				if(err.request.status === 404) {
					throw new Error('participate.code.notExist');
				} else if (err.request.status === 409){
					throw new Error('participate.code.notOpen');
				} else {
					console.error(err, err.response);
					throw new Error('participate.code.failure');
				}
			});
	}
	
	const joinSession = async (sessionId: string): Promise<any|null> => {
		return axios.get(apiUrl + '/participate/'+sessionId+'/join')
			.then((response) => {
				return {token: response.data.token, sessionId: response.data.sessionId};
			})
			.catch(async (err) => {
				if(err.request.status === 404) {
					throw new Error('participate.code.notExist');
				} else if (err.request.status === 409){
					throw new Error('participate.code.notOpen');
				} else {
					console.error(err, err.response);
					throw new Error('participate.code.failure');
				}
			});
	}
	
	const getSession = async (): Promise<Session> => {
		return axios.get(apiUrl + '/participate/session', _getAuthHeader())
			.then((response) => {
				return response.data.session;
			})
			.catch(async (err) => {
				if(err.request.status === 404) {
					throw new Error('participate.code.notExist');
				} else {
					console.error(err, err.response);
					throw new Error('participate.code.failure');
				}
			});
	}
	
	const sendAnswer = async (data: {questionId: string, answer: number[]|number}) : Promise<boolean> => {
		return axios.post(apiUrl + '/participate/'+data.questionId, {answer: data.answer}, _getAuthHeader())
			.then(() => {
				return true;
			})
			.catch(async (err) => {
				if(err.request.status === 404) {
					if(err.request.message === 'Question not found.') {
						throw new Error('participate.question.notExist');
					} else {
						throw new Error('participate.code.notExist');
					}
				} else if (err.request.status === 409){
					if(err.request.message === 'Question isClosed.') {
						throw new Error('participate.question.notOpen');
					} else {
						throw new Error('participate.code.notOpen');
					}
				} else {
					console.error(err, err.response);
					throw new Error('participate.question.failure');
				}
			});
	}
	
	return {getSessionIdForAccessCode, joinSession, getSession, sendAnswer};
	
}