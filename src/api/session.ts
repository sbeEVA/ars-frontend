import axios from "axios";
import {Session} from '@/model/Session';

export default function sessionAPI(apiUrl: string, token: string) {
	const _getAuthHeader = () => {
		return {
			headers: {
				authorization: 'Bearer ' + token,
			}
		}
	}
	
	const getSessionsOfUser = async (): Promise<Session[]> => {
		return axios.get(apiUrl + '/session', _getAuthHeader())
			.then(async(response) => {
				return Object.entries(response.data.sessions).map((value: any) => {
					return value[1] as Session;
				});
			})
			.catch(async (err) => {
				if(err.request.status === 401) {
					throw new Error('invalidUser');
				} else {
					console.error(err, err.response);
				}
				return [];
			})
	}
	
	const getSessionById = (id: string): Promise<Session|null> => {
		return axios.get(apiUrl + '/session/'+id, _getAuthHeader())
			.then((response) => {
				return response.data.session;
			})
			.catch(async (err) => {
				if(err.request.status === 401) {
					throw new Error('invalidUser');
				} else {
					console.error(err, err.response);
				}
				return null;
			});
	}
	
	const insertSession = async (data: any ): Promise<string> => {
		return axios.put(apiUrl+'/session', data, _getAuthHeader())
			.then(async (result) => {
				return result.data.sessionId;
			})
			.catch(async (err) => {
				if(err.request.status === 401) {
					throw new Error('invalidUser');
				} else {
					console.error(err, err.response);
					throw new Error('sessions.add.failure');
				}
			});
	};
	
	const updateSession = async (id: string, data: any ): Promise<boolean> => {
		return axios.patch(apiUrl+'/session/'+id, data, _getAuthHeader())
			.then(async () => {
				return true;
			})
			.catch(async (err) => {
				if(err.request.status === 401) {
					throw new Error('invalidUser');
				} else {
					console.error(err, err.response);
					throw new Error('sessions.edit.updateFailure');
				}
			});
	}
	
	const deleteSession = async (id: string): Promise<boolean> => {
		return await axios.delete(apiUrl+'/session/'+id, _getAuthHeader()).then(async () => {
			return true;
		}).catch(async (err: any) => {
			if(err.request.status === 401) {
				throw new Error('invalidUser');
			} else {
				console.error(err, err.response);
				throw new Error('sessions.edit.deleteFailure');
			}
		});
	}
	
	return {getSessionsOfUser, getSessionById, insertSession,  updateSession, deleteSession};
	
}