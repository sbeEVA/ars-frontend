export interface SessionQuestion {
	_id: string;
	question: Question;
	isOpen: boolean;
	shareResults: boolean;
	results: any[];
}

export interface Question {
	_id: string;
	text: string;
	answers: string[];
}