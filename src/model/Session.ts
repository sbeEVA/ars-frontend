import {SessionQuestion} from "@/model/Question";

export interface Session {
	_id: string;
	name: string;
	accessCode: string;
	isOpen: boolean;
	userId: string;
	createdAt: Date;
	updatedAt: Date;
	questions: SessionQuestion[];
}