import { createI18n } from "vue-i18n";
import German from './de-DE.json';

export default createI18n({
	legacy: false, // you must set `false`, to use Composition API
	locale: 'de-DE', // set locale
	fallbackLocale: 'de-DE', // set fallback locale
	globalInjection: true,
	messages: {
		'de-DE': German
	},
});