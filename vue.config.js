module.exports = {
	devServer: {
		allowedHosts: 'all',
		port: 8080
	},
	publicPath: "/"
};