# evasys ARS
This is the frontend application of evasys ARS, a tool for receiving instant feedback from a group of persons. You can quickly create questions, let participants join your session and share the results, if you want to.

This code compiles for three platforms: Web, Android and iOS.

## Starting the application
Go to `src/config/development.json` to set up the local frontend and backend URL

Go to `src/config/production.json` to set up the paths for the production environment. This has to change in the future to use environment variables of the deployment server.

Take care that the backend is running and accessible at the configured URL.

To start the frontend, call `npm run serve`. To build a production version, run `npm run build`

## Build the app
Run the following commands:
1. Compile the project with `ionic build`
2. If something in the native code has changed, run `ionic cap sync`
3. Copy the build to the native projects with `ionic cap copy`
4. Now open the project in the native IDE with `ionic cap open android` or `ionic cap open ios`
5. If you are looking for the console.log for the android app, you can start Chrome and type: `chrome://inspect`. With this, you can watch an usb device or the emulated android phone on your computer. 

## Testing
This app only supports cypress tests. To start them, run `npm run test:e2e`. No backend is required to perform the tests.